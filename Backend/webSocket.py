from flask import Flask
from flask_socketio import SocketIO
from flask_cors import CORS
import sqlite3
import time
import threading

# 創建Flask並啟用CORS
app = Flask(__name__)
CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")  # 設置允許來源

active_thread = None  # 用於執行續的活動紀錄
active_thread_lock = threading.Lock()  # 多執行續操作active_thread的安全性


def get_latest_data(name, localAddr, No):  # 取得最新數據
    conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
    cursor = conn.cursor()
    # 依時間排序 取出一筆最新的
    cursor.execute(
        """
        SELECT * FROM mqtt_data
        WHERE label = ? AND LocalAddr = ? AND No = ?
        ORDER BY timestamp DESC
        LIMIT 1
    """,
        (name, localAddr, No),
    )
    data = cursor.fetchone()
    cursor.close()
    conn.close()
    return data


def send_latest_data(name, localAddr, No): # 巢狀迴圈 每5秒就取出最新的該筆並emit
    global active_thread
    while True:
        with active_thread_lock:
            if active_thread != threading.current_thread():
                return
        data = get_latest_data(name, localAddr, No)
        if data:
            socketio.emit(
                "latest_data",
                {
                    "timestamp": data[2],
                    "label": data[3],
                    "value": data[4],
                },
                namespace="/",
            )
        time.sleep(5)


# def send_latest_data_thread(name, localAddr, No):
#     global active_thread
#     while active_thread == threading.current_thread():
#         data = get_latest_data(name, localAddr, No)
#         if data:
#             socketio.emit(
#                 "latest_data",
#                 {
#                     "timestamp": data[2],
#                     "label": data[3],
#                     "value": data[4],
#                 },
#                 namespace="/",
#             )
#         time.sleep(5)


@socketio.on("getNowData")
def getNowData(data):
    # 前端會傳來現在是哪個drawer被打開 再運行多線程 emit相對應的數據
    name = data.get("name")
    device = data.get("device")
    No = data.get("No")

    conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/dashboard.db")
    cursor = conn.cursor()
    query = (
        "SELECT LocalAddr from dashboard_data WHERE No = ? AND Label = ? AND Device = ?"
    )

    cursor.execute(query, (No, name, device))
    result = cursor.fetchone()
    cursor.close()
    conn.close()

    localAddr = result[0]

    # 檢查是否有線程
    global active_thread

    with active_thread_lock:
        if active_thread is not None:
            active_thread = None

    active_thread = threading.Thread(
        target=send_latest_data, args=(name, localAddr, No)
    )
    active_thread.start()


@socketio.on("connect")
def on_connect():
    print("Client connected")


@socketio.on("disconnect")
def on_disconnect():
    print("Client disconnected")
    global active_thread
    with active_thread_lock:
        active_thread = None


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", port=8765, allow_unsafe_werkzeug=True)
