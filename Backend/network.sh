#!/bin/bash
# Apache user have sudo
sudo ip addr flush dev eth0 && sudo ip addr flush dev eth1
sudo ifdown eth0 && sudo ifup eth0
sudo ifdown eth1 && sudo ifup eth1
ps -ef | grep "save_to_db.py" | grep -v grep | awk '{print $2}' | xargs kill -9
kill -9 $(ps -ef | grep "api.py" | grep -v grep | awk '{print $2}')
ps -ef | grep "webSocket.py" | grep -v grep | awk '{print $2}' | xargs kill -9
/usr/bin/python3 /usr/share/apache2/default-site/htdocs/save_to_db.py >> /var/log/db.log &
/usr/bin/python3 /usr/share/apache2/default-site/htdocs/api.py >> /var/log/api.log &
/usr/bin/python3 /usr/share/apache2/default-site/htdocs/webSocket.py >> /var/log/webSocket.log &