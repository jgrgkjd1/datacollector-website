#!/bin/bash

filepath="/etc/network/interfaces"
while getopts "o:n:e:1:7:t:w:i:2:8:3456" opt; do
	case $opt in
	o)
		#static eth0 ip
		new_ip="	address $OPTARG"
		# grep -n address 就可以找到line number
		ip_number="28"
		echo "$OPTARG"
		sed -i "${ip_number}s/.*/$new_ip/" $filepath
		;;
	n)
		#static eth0 netmask
		new_netmask="	netmask $OPTARG"
		#interface有變就要重新找，`grep -n netmask`
		netmask_number="29"
		sed -i "${netmask_number}s/.*/$new_netmask/" $filepath
		;;
	e)
		#static eth0 network
		new_network="	network $OPTARG"
		#interface有變就要重新找
		network_number="30"

		sed -i "${network_number}s/.*/$new_network/" $filepath
		;;
	1)
		#static eth0 gateway
		new_gateway="	gateway $OPTARG"
		gateway_number="31"

		sed -i "${gateway_number}s/.*/$new_gateway/" $filepath
		;;
	t)
		#static eth1 ip
		new_ip1="	address $OPTARG"
		ip1_number="35"
		sed -i "${ip1_number}s/.*/$new_ip1/" $filepath
		;;
	w)
		#static eth1 netmask
		new_netmask1="	netmask $OPTARG"
		netmask1_number="36"

		sed -i "${netmask1_number}s/.*/$new_netmask1/" $filepath
		;;
	i)
		#static eth1 network
		new_network1="	network $OPTARG"
		network1_number="37"

		sed -i "${network1_number}s/.*/$new_network1/" $filepath
		;;
	2)
		#static eth1 gateway
		new_gateway1="	gateway $OPTARG"
		gateway1_number="38"

		sed -i "${gateway1_number}s/.*/$new_gateway1/" $filepath
		;;
	8)
		new_gateway1="	#gateway $OPTARG"
		gateway1_number="38"
		
		sed -i "${gateway1_number}s/.*/$new_gateway1/" $filepath
		;;
	3)
		#dhcp eth0
		old_ip="$(awk '/address/ && NR==28 {print $2}' $filepath)"
		old_netmask="$(awk '/netmask/ && NR==29 {print $2}' $filepath)"
		old_network="$(awk '/gateway/ && NR==30 {print $2}' $filepath)"
		old_gateway="$(awk '/gateway/ && NR==31 {print $2}' $filepath)"

		change_dhcp="iface eth0 inet dhcp"
		mark_ip="	#address $old_ip"
		mark_netmask="	#netmask $old_netmask"
		mark_network="	#network $old_network"
		mark_gateway="	#gateway $old_gateway"

		sed -i "27s/.*/$change_dhcp/" $filepath
		sed -i "28s/.*/$mark_ip/" $filepath
		sed -i "29s/.*/$mark_netmask/" $filepath
		sed -i "30s/.*/$mark_network/" $filepath
		sed -i "31s/.*/$mark_gateway/" $filepath
		;;
	4)
		#dhcp eth1
		old_ip1="$(awk '/address/ && NR==35 {print $2}' $filepath)"
		old_netmask1="$(awk '/netmask/ && NR==36 {print $2}' $filepath)"
		old_network1="$(awk '/network/ && NR==37 {print $2}' $filepath)"
		old_gateway1="$(awk '/gateway/ && NR==38 {print $2}' $filepath)"

		change_dhcp1="iface eth1 inet dhcp"
		mark_ip1="	#address $old_ip1"
		mark_netmask1="	#netmask $old_netmask1"
		mark_network1="	#network $old_network1"
		mark_gateway1="	#gateway $old_gateway1"

		sed -i "34s/.*/$change_dhcp1/" $filepath
		sed -i "35s/.*/$mark_ip1/" $filepath
		sed -i "36s/.*/$mark_netmask1/" $filepath
		sed -i "37s/.*/$mark_network1/" $filepath
		sed -i "38s/.*/$mark_gateway1/" $filepath
		;;
	5)
		# 每執行一次就取代該行，確保狀態在static
		sed -i "27s/.*/iface eth0 inet static/" $filepath
		;;
	6)
		# 每執行一次就取代該行，確保狀態在static
		sed -i "34s/.*/iface eth1 inet static/" $filepath
		;;
	esac
done
