import paho.mqtt.client as mqtt
import sqlite3
import time
import datetime

# 讀取設定檔內容
with open(
    "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_device.txt", "r"
) as f:
    content = f.read()

# 創建列表
config_items = [
    "mqtt_broker_hostname",
    "mqtt_broker_port",
    "mqtt_topic_data",
]  # 總共要取出這三個
config_values = {}

mqtt_broker_hostname = ""
mqtt_broker_port = ""
mqtt_topic_data = ""

# 遍歷列表並提取對應的值 存進config_values
for item in config_items:
    keyword = f"{item}="
    start_index = content.find(keyword)
    if start_index != -1:
        start_index += len(keyword)
        end_index = content.find("\n", start_index)
        if end_index != -1:
            config_values[item] = content[start_index:end_index].strip()
        else:
            config_values[item] = "Value not found"
    else:
        config_values[item] = "Not found"

# 從config_values中取得MQTT相關的值
for item, value in config_values.items():
    if item == "mqtt_broker_hostname":
        mqtt_broker_hostname = value
    elif item == "mqtt_broker_port":
        mqtt_broker_port = int(value)
    elif item == "mqtt_topic_data":
        mqtt_topic_data = value

# SQLite資料庫設定
DATABASE_FILE = "/usr/share/apache2/default-site/htdocs/data.db"


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(mqtt_topic_data)  # 連接成功時訂閱MQTT主題


def on_message(client, userdata, msg):  # 收到訊息時
    payload = msg.payload.decode("utf-8")
    print("Received message: " + payload)

    # 解析payload，提取所需的值
    data = payload.split(",")
    no = data[0]
    register = data[1]
    timestamp = data[3]
    label = data[4]
    value = data[7]
    print("No:", no)
    print("Timestamp:", timestamp)
    print("Label:", label)
    print("Value:", value)

    # 將數據寫入SQLite資料庫
    conn = sqlite3.connect(DATABASE_FILE)
    cursor = conn.cursor()

    cursor.execute(
        "INSERT INTO mqtt_data (No,LocalAddr,timestamp, label, value) VALUES (?,?,?,?,?)",
        (no, register, timestamp, label, value),
    )

    conn.commit()
    conn.close()


def clear_database(): # 定時清空database
    now = datetime.datetime.now()

    # 獲取昨天的日期和當下時間
    yesterday = now - datetime.timedelta(days=1)

    # 計算昨天同一時間和昨天同一時間-30分鐘
    yesterday_same_time = datetime.datetime(
        yesterday.year, yesterday.month, yesterday.day, now.hour, now.minute
    )
    yesterday_30_minutes_ago = yesterday_same_time - datetime.timedelta(minutes=30)

    # 刪除昨天同一時間和昨天同一時間-30分鐘之間的數據
    conn = sqlite3.connect(DATABASE_FILE)
    cursor = conn.cursor()
    cursor.execute(
        "DELETE FROM mqtt_data WHERE timestamp >= ? AND timestamp < ?",
        (yesterday_30_minutes_ago, yesterday_same_time),
    )
    conn.commit()
    conn.close()


# 設置MQTT
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(mqtt_broker_hostname, mqtt_broker_port, 60)

client.loop_start()

while True:
    # 每天00:00時清空資料庫，重新紀錄新的一天數據
    # now = datetime.datetime.now()
    # if now.hour == 0 and now.minute == 0:
    #     clear_database()
    # time.sleep(60)
    time.sleep(1800)  # 每1800秒檢查一次
    clear_database()
