import sqlite3
from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
from datetime import datetime, timedelta
import re
import os
import multiprocessing
import time
import requests
import threading
import subprocess
import re
import shutil
import zipfile
from werkzeug.utils import secure_filename

app = Flask(__name__)
CORS(app)


def calculate_average_value(cursor, start_time, end_time):
    # 執行SQL查詢計算每個label的平均值（去除value欄位中的單位後再計算）
    query = "SELECT label, AVG(CAST(REPLACE(value, '°C', '') AS FLOAT)) AS average_value FROM mqtt_data WHERE timestamp BETWEEN ? AND ? GROUP BY label"
    cursor.execute(query, (start_time, end_time))
    result = cursor.fetchall()
    return result


@app.route("/getAverage", methods=["POST"])
def get_average_by_time():
    try:
        data = request.json  # 傳的是JSON要先解析

        start_time_str = data.get("start_time")
        end_time_str = data.get("end_time")

        start_time = datetime.strptime(start_time_str, "%Y-%m-%d %H:%M")
        end_time = datetime.strptime(end_time_str, "%Y-%m-%d %H:%M")

        # 建立與SQLite資料庫的連接
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        # 取得指定時間範圍內每個label的平均值
        result = calculate_average_value(cursor, start_time, end_time)

        # 關閉資料庫連接
        conn.close()

        # 將結果轉換為字典格式，並回傳JSON格式的回應
        average_dict = {}
        for row in result:
            label = row[0]
            average_value = round(row[1], 2)
            average_dict[label] = average_value

        return jsonify(average_dict)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getToday", methods=["GET"])
def getDate():
    try:
        today = datetime.now().date()

        # 計算今天的 00:00
        today_midnight = datetime(today.year, today.month, today.day, 0, 0)

        # 計算每三十分鐘的時間間隔
        time_interval = timedelta(minutes=30)

        # 生成需要的多個時間點
        times = []
        current_time = today_midnight
        while current_time <= today_midnight + timedelta(days=1):
            times.append(current_time.strftime("%H:%M"))
            current_time += time_interval

        return jsonify({"times": times})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getData", methods=["GET"])
def getData():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        # 取得現在時間和一小時前的時間
        now = datetime.now()
        one_hour_ago = now - timedelta(hours=1)

        # 只取出label為"TEMP"且在過去一小時內的數據
        cursor.execute(
            "SELECT * FROM mqtt_data WHERE label='TEMP' AND timestamp >= ? ORDER BY timestamp",
            (one_hour_ago,),
        )

        data = []
        for row in cursor.fetchall():
            no, register, timestamp, label, value = row
            value = remove_symbol_from_value(value)
            data.append({"timestamp": timestamp, "label": label, "value": value})

        conn.close()

        return jsonify(data)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/dashboardData", methods=["GET"])
def testData():
    try:
        dashboard_conn = sqlite3.connect(
            "/usr/share/apache2/default-site/htdocs/dashboard.db"
        )
        dashboard_cursor = dashboard_conn.cursor()

        # 取得 No, Label, LocalAddr, Device 資料
        dashboard_query = """
            SELECT No, Label, LocalAddr, Device
            FROM dashboard_data
        """
        dashboard_cursor.execute(dashboard_query)
        dashboard_data = dashboard_cursor.fetchall()

        dashboard_cursor.close()
        dashboard_conn.close()

        # 取得現在時間和一小時前的時間
        now = datetime.now()
        one_hour_ago = now - timedelta(hours=1)

        data_conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        data_cursor = data_conn.cursor()

        # 篩選條件，使用 IN 來指定需要匹配的 Label
        label_condition = "label IN (" + ",".join(["?" for _ in dashboard_data]) + ")"
        localAddr_condition = " OR ".join(["LocalAddr = ?" for _ in dashboard_data])
        no_condition = " OR ".join(["No = ?" for _ in dashboard_data])
        print(label_condition)
        query = f"""
            SELECT label, timestamp, value,No
            FROM mqtt_data
            WHERE (timestamp >= ? AND timestamp <= ?) AND ({label_condition}) AND ({localAddr_condition}) AND ({no_condition})
            ORDER BY label ASC, timestamp ASC
        """

        # 取得所有需要匹配的
        no = [row[0] for row in dashboard_data]
        labels = [row[1] for row in dashboard_data]
        localAddr = [row[2] for row in dashboard_data]

        # 將 labels 插入到 query 中，將 one_hour_ago 和 now 也加入到 query 的參數中
        data_cursor.execute(query, [one_hour_ago, now] + labels + localAddr + no)
        data = data_cursor.fetchall()

        data_cursor.close()
        data_conn.close()

        result = {}
        for row in data:
            label = row[0]
            timestamp = row[1]
            value = row[2]

            if label not in result:
                result[label] = []

            result[label].append({"x": str(timestamp), "y": str(value)})

        # 將資料轉換成陣列形式，並將Device加入到每個元素中
        chart_data = [
            {
                "name": label,
                "Device": data[3],
                "data": result[label],
                "No": str(data[0]),
            }
            for label, data in zip(labels, dashboard_data)
        ]

        return jsonify(chart_data)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/nowData", methods=["GET"])
def nowData():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        # 只取出label為"TEMP"的數據，按照timestamp降序排序，並只返回最新的一筆數據
        cursor.execute(
            "SELECT * FROM mqtt_data WHERE label='TEMP' ORDER BY timestamp DESC LIMIT 1"
        )

        data = cursor.fetchone()
        if data:
            no, register, timestamp, label, value = data
            value = remove_symbol_from_value(value)
            result = {"timestamp": timestamp, "label": label, "value": value}
        else:
            result = {}

        conn.close()

        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/addDevice", methods=["POST"])
def addDevice():
    try:
        data = request.json

        company = data.get("Company")
        model = data.get("Model")
        Class = data.get("Class")
        # 組合字串
        combined_str = f"{company}_{model}_{Class}"

        # 用正規表達式檢查是否符合命名規則
        if not checkTableName(combined_str):
            result = {"status": "Invalid name format"}
            return jsonify(result)

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        # 檢查table是否存在了
        if tableExists(cursor, combined_str):
            result = {"status": "Already exists"}
            cursor.close()
            conn.close()
            return jsonify(result)

        create_table = f"CREATE TABLE IF NOT EXISTS {combined_str} (No INTEGER,MappingAddr TEXT,WordLen TEXT,LocalAddr TEXT,Label TEXT,FunctionCode TEXT,Format TEXT,UnitVal TEXT,Offset TEXT,UnitStr TEXT,PollMS TEXT,SlaveAddr TEXT,LinkInfo TEXT,MB_TCP_IP TEXT,MB_TCP_PORT TEXT)"

        cursor.execute(create_table)
        conn.commit()
        cursor.close()
        conn.close()

        result = {"status": "Successful"}
        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


def checkTableName(table_name):  # 檢查是否符合sqlite命名規則
    return re.match("^[a-zA-Z_][a-zA-Z0-9_]*$", table_name) is not None


def tableExists(cursor, table_name):  # 檢查table是否存在
    cursor.execute(
        f"SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}' COLLATE NOCASE"
    )
    return cursor.fetchone() is not None


@app.route("/delDevice", methods=["POST"])  # 刪除裝置
def delDevice():
    try:
        data = request.json
        Class = data.get("Class")
        company = data.get("Company")
        model = data.get("Model")

        combined_str = f"DROP TABLE IF EXISTS {company}_{model}_{Class};"

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        cursor.execute(combined_str)

        conn.commit()
        conn.close()

        return jsonify({"status": "Delete Successful"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/editDevice", methods=["POST"])
def editDevice():
    try:
        data = request.json
        old_company = data[0].get("old").get("Company")
        old_model = data[0].get("old").get("Model")
        old_class = data[0].get("old").get("Class")
        new_company = data[0].get("new").get("Company")
        new_model = data[0].get("new").get("Model")
        new_class = data[0].get("new").get("Class")

        combined_old = f"{old_company}_{old_model}_{old_class}"
        combined_new = f"{new_company}_{new_model}_{new_class}"
        # 如果新舊相同就返回 會有點難編輯
        if combined_old == combined_new:
            return jsonify({"status": "You didn't edit !"})

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        cursor.execute(f"ALTER TABLE {combined_old} RENAME TO {combined_new};")
        conn.commit()

        cursor.close()
        conn.close()
        # 連帶更新dashboard.db的值，以確保名稱同步
        conn1 = sqlite3.connect("/usr/share/apache2/default-site/htdocs/dashboard.db")
        cursor1 = conn1.cursor()

        update_query = f"UPDATE dashboard_data SET Device = ? WHERE Device = ?"
        cursor1.execute(update_query, (combined_new, combined_old))
        conn1.commit()

        cursor1.close()
        conn1.close()

        return jsonify({"status": "Edit Successful"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getTableData", methods=["GET"])
def getTableData():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        # 取得所有表格名稱
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = cursor.fetchall()

        # 建立用來存放結果的列表
        results = []

        # 遍歷表格名稱並按照底線分割
        for table in tables:
            table_name = table[0]
            if "_" in table_name:
                split_table_name = table_name.split("_")
                result = {
                    "Company": split_table_name[0],
                    "Model": split_table_name[1],
                    "Class": split_table_name[2],
                }
                results.append(result)
        cursor.close()
        conn.close()

        return jsonify(results)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/addRegister", methods=["POST"])
def addRegister():
    data = request.json
    Class = data.get("Class")
    company = data.get("Company")
    model = data.get("Model")
    combined_str = f"{company}_{model}_{Class}"
    Format = data.get("Format")
    FunCode = data.get("FunCode")
    label = data.get("Label")
    linkInfo = data.get("LinkInfo")
    LocalAddr = data.get("LocalAddr")
    offset = data.get("Offset")
    pollms = data.get("Pollms")
    slaveAddr = data.get("SlaveAddr")
    unitStr = data.get("UnitStr")
    unitVal = data.get("Unitval")
    wordLen = data.get("WordLen")
    mb_ip = data.get("MB_IP")
    mb_port = data.get("MB_PORT")

    conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
    search = f"SELECT No FROM {combined_str} ORDER BY No DESC LIMIT 1"
    cursor = conn.cursor()

    cursor.execute(search)
    last_row = cursor.fetchone()  # 取得查詢結果的第一筆資料

    if last_row:
        last_no = last_row[0]
        last_no = last_no + 1
    else:
        last_no = 1

    try:
        columns = "No,MappingAddr,WordLen,LocalAddr,Label,FunctionCode,Format,UnitVal,Offset,UnitStr,PollMS,SlaveAddr,LinkInfo,MB_TCP_IP,MB_TCP_PORT"
        query = f"INSERT INTO {combined_str} ({columns}) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        cursor.execute(
            query,
            (
                last_no,
                LocalAddr,
                wordLen,
                LocalAddr,
                label,
                FunCode,
                Format,
                unitVal,
                offset,
                unitStr,
                pollms,
                slaveAddr,
                linkInfo,
                mb_ip,
                mb_port,
            ),
        )

        conn.commit()
        print("Data add successful")
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500
    finally:
        cursor.close()
        conn.close

        response = {"status": "Add Successful"}
        return jsonify(response)


@app.route("/getRegisterData", methods=["POST"])
def getRegisterData():
    try:
        data = request.json
        company = data.get("Company")
        model = data.get("Model")
        Class = data.get("Class")

        query = f"SELECT * FROM {company}_{model}_{Class}"

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        cursor.execute(query)

        # 取得查詢結果的欄位名稱
        column_names = [description[0] for description in cursor.description]

        # 將查詢結果轉換為字典的形式，並將這些字典組成一個列表
        all_data = []
        for row in cursor.fetchall():
            row_dict = dict(zip(column_names, row))
            all_data.append(row_dict)

        cursor.close()
        conn.close()

        return jsonify(all_data)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/updateRegister", methods=["POST"])
def updateRegister():
    try:
        data = request.json

        company = data.get("Company")
        model = data.get("Model")
        Class = data.get("Class")
        combined_str = f"{company}_{model}_{Class}"

        no = data.get("No")
        new_data = {
            "MappingAddr": data.get("LocalAddr"),
            "WordLen": data.get("WordLen"),
            "LocalAddr": data.get("LocalAddr"),
            "Label": data.get("Label"),
            "FunctionCode": data.get("FunctionCode"),
            "Format": data.get("Format"),
            "UnitVal": data.get("UnitVal"),
            "Offset": data.get("Offset"),
            "UnitStr": data.get("UnitStr"),
            "PollMS": data.get("PollMS"),
            "SlaveAddr": data.get("SlaveAddr"),
            "LinkInfo": data.get("LinkInfo"),
            "MB_TCP_IP": data.get("MB_TCP_IP"),
            "MB_TCP_PORT": data.get("MB_TCP_PORT"),
        }

        # 構建 SQL 語句
        query = f"UPDATE {combined_str} SET "
        query += ", ".join([f"{key}=?" for key in new_data.keys()])
        query += " WHERE No=?"

        # 建立更新資料的值列表，用來代替 SQL 語句中的問號佔位符
        values = list(new_data.values()) + [no]

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        # 執行更新操作
        cursor.execute(query, values)
        conn.commit()

        cursor.close()
        conn.close()

        return jsonify({"status": "Data updated successfully!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/deleteRegister", methods=["POST"])  # 刪除暫存器
def deleteRegister():
    try:
        data = request.json
        no = data[0].get("selectRow").get("No")
        company = data[0].get("needCombined").get("Company")
        model = data[0].get("needCombined").get("Model")
        Class = data[0].get("needCombined").get("Class")

        combined_str = f"{company}_{model}_{Class}"

        query = f"DELETE FROM {combined_str} WHERE No=?"

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        cursor.execute(query, (no,))
        conn.commit()

        cursor.close()
        conn.close()

        return jsonify({"status": "Delete Successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getTable", methods=["GET"])  # 取得所有表格名稱
def getTable():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
        table_names = [row[0] for row in cursor.fetchall()]

        cursor.close()
        conn.close()

        return jsonify(table_names)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getRegister", methods=["POST"])  # 運用表格名稱來取得所有暫存器
def getRegister():
    try:
        data = request.json
        table = data.get("table")

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        query = f"SELECT No, LocalAddr, Label FROM {table}"

        cursor.execute(query)

        # 獲取查詢結果中的所有行
        rows = cursor.fetchall()

        # 構建回傳的字典，其中 key 為 "No, LocalAddr, Label"，value 為 "LocalAddr, Label"
        result = {
            f"{row[1]}, {row[2]}": f"{row[0]}, {row[1]}, {row[2]}" for row in rows
        }

        cursor.close()
        conn.close()

        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/setDashboard", methods=["POST"])  # 設定要展示的dashboard名稱
def setDashboard():
    try:
        data = request.json
        device = data.get("Device")
        payload = data.get("Register")

        payload_list = payload.split(",")
        no = payload_list[0].strip()
        register = payload_list[1].strip()
        label = payload_list[2].strip()
        # 先檢查database在不在
        if not os.path.exists("/usr/share/apache2/default-site/htdocs/dashboard.db"):
            conn = sqlite3.connect(
                "/usr/share/apache2/default-site/htdocs/dashboard.db"
            )
            cursor = conn.cursor()
            cursor.execute(
                "CREATE TABLE dashboard_data (No INTEGER,LocalAddr TEXT,Label TEXT,Device TEXT)"
            )
            conn.commit()
            cursor.close()
            conn.close()

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/dashboard.db")
        cursor = conn.cursor()
        # 有這些欄位 並將前端傳來要設置的插入
        colums = "No,LocalAddr,Label,Device"
        query = f"INSERT INTO dashboard_data ({colums}) VALUES (?,?,?,?)"
        cursor.execute(query, (no, register, label, device))
        conn.commit()
        cursor.close()
        conn.close()

        return jsonify({"status": "Set dashboard successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getDashboardTable", methods=["GET"])
def getDashboardTable():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/dashboard.db")
        cursor = conn.cursor()

        # 從 dashboard_data table 取得 Label, localAddr, Device 資料
        query = """
            SELECT Label, localAddr, Device
            FROM dashboard_data
        """
        cursor.execute(query)
        data = cursor.fetchall()

        # 將資料轉換成指定格式的字典列表
        result = []
        for row in data:
            label, localAddr, device = row
            result.append({"Label": label, "localAddr": localAddr, "Device": device})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500
    finally:
        cursor.close()
        conn.close()
        return jsonify(result)


@app.route("/delDashboard", methods=["POST"])  # 刪除配置的dashboard
def delDashboard():
    try:
        data = request.json
        device = data.get("Device")
        label = data.get("Label")
        localAddr = data.get("localAddr")

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/dashboard.db")
        cursor = conn.cursor()
        delete_query = """
            DELETE FROM dashboard_data
            WHERE Device = ? AND Label = ? AND LocalAddr = ?
        """
        cursor.execute(delete_query, (device, label, localAddr))
        conn.commit()

        cursor.close()
        conn.close()

        return jsonify({"status": "Delete successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


child_process = None  # 多線程 以處理檢查發送警訊的迴圈


@app.route("/setNotify", methods=["POST"])  # 寫入line notify相關設置
def setNotify():
    global child_process
    data = request.json
    status = data.get("status")
    token = data.get("token")
    frequency = data.get("frequency")

    conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
    cursor = conn.cursor()
    query = f"UPDATE notify_data SET status = ?,token = ?,frequency = ?"

    cursor.execute(query, (status, token, frequency))
    conn.commit()

    cursor.close()
    conn.close()

    # 如果使用者是設置true
    if status:
        # 先檢查child_process的狀態 重置再啟用
        if child_process is not None:
            child_process.terminate()
            child_process.join()
            print("Previous process stopped.")
            child_process = None

        child_process = multiprocessing.Process(target=notify_process)
        child_process.start()
        print("New process started.")
    else:
        # 如果有正在運行的子進程，則停止它
        if child_process is not None:
            child_process.terminate()
            child_process.join()
            print("Process stopped.")
            child_process = None
        else:
            print("No process is currently running.")

    return jsonify({"status": "Set Successful!"})


def notify_process():  # line notify的子線程
    try:
        clear_interval = frequency * 5  # 清除時間 = 間隔x5 避免過短造成不必要的處理
        sent_data = set()  # 建立一個集合 將data弄成一包 不要每個有no response就傳 可能會超出規範的上限次數

        def clear_sent_data():
            sent_data.clear()
            clear_timer = threading.Timer(clear_interval, clear_sent_data)
            clear_timer.start()
            print("data clear")

        clear_timer = threading.Timer(clear_interval, clear_sent_data)
        clear_timer.start()

        while True:
            # 向line notify API 請求
            url = "https://notify-api.line.me/api/notify"
            conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
            cursor = conn.cursor()
            cursor.execute("SELECT token, frequency FROM notify_data")
            result = cursor.fetchone()
            token = result[0]
            frequency = result[1]
            headers = {"Authorization": "Bearer " + token}

            query = f"SELECT LocalAddr, timestamp, label, value FROM mqtt_data WHERE value='NO RESPONSE'"
            cursor.execute(query)
            rows = cursor.fetchall()
            cursor.close()
            conn.close()

            # 合併發送內容 並加入集合內
            for row in rows:
                localAddr, timestamp, label, value = row
                data_id = f"{localAddr}-{timestamp}-{label}"
                if data_id not in sent_data:
                    result_str = f"{localAddr}，{timestamp}，{label}，{value}"
                    data = {
                        "message": result_str,
                    }
                    requests.post(url, headers=headers, data=data)
                    sent_data.add(data_id)
            # 使用者自訂頻率
            time.sleep(frequency)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/notifyStatus", methods=["GET"])  # 取得現在notify狀態
def notifyStatus():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()
        query = f"SELECT * FROM notify_data"

        cursor.execute(query)
        data = cursor.fetchone()

        columns = [column[0] for column in cursor.description]
        data_dict = dict(zip(columns, data))

        return jsonify(data_dict)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/setEthernet", methods=["POST"])  # 設定網路接口
# 在這邊判斷前端設置的配置 將argument發送給ip.sh 透過linux command來做替換修改 最後會將網路配置一併寫入database中
def setEthernet():
    try:
        data = request.json
        mode0 = data.get("mode0")
        ip0 = data.get("ip0")
        netmask0 = data.get("netmask0")
        network0 = data.get("network0")
        gateway0 = data.get("gateway0")
        mode1 = data.get("mode1")
        ip1 = data.get("ip1")
        netmask1 = data.get("netmask1")
        network1 = data.get("network1")
        gateway1 = data.get("gateway1")
        mark = data.get("mark")

        if mode0 == "static":
            ip0_static_cmd = [
                "/usr/share/apache2/default-site/htdocs/ip.sh",
                "-5",
                "-o",
                ip0,
                "-n",
                netmask0,
                "-e",
                network0,
                "-1",
                gateway0,
            ]
            result = subprocess.run(
                ip0_static_cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
            )
            if result.returncode != 0:
                return jsonify({"status": result.stderr}), 500
        else:
            ip0_dhcp_cmd = ["/usr/share/apache2/default-site/htdocs/ip.sh", "-3"]
            result = subprocess.run(
                ip0_dhcp_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
            )
            if result.returncode != 0:
                return jsonify({"status": result.stderr}), 500

        if mode1 == "static":
            if mark:
                ip1_static_cmd = [
                    "/usr/share/apache2/default-site/htdocs/ip.sh",
                    "-6",
                    "-t",
                    ip1,
                    "-w",
                    netmask1,
                    "-i",
                    network1,
                    "-8",
                    gateway1,
                ]
                result1 = subprocess.run(
                    ip1_static_cmd,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    text=True,
                )
                if result1.returncode != 0:
                    return jsonify({"status": result1.stderr}), 500
            else:
                ip1_static_cmd = [
                    "/usr/share/apache2/default-site/htdocs/ip.sh",
                    "-6",
                    "-t",
                    ip1,
                    "-w",
                    netmask1,
                    "-i",
                    network1,
                    "-2",
                    gateway1,
                ]
                result1 = subprocess.run(
                    ip1_static_cmd,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    text=True,
                )
                if result1.returncode != 0:
                    return jsonify({"status": result1.stderr}), 500
        else:
            ip1_dhcp_cmd = ["/usr/share/apache2/default-site/htdocs/ip.sh", "-4"]
            result1 = subprocess.run(
                ip1_dhcp_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
            )
            if result1.returncode != 0:
                return jsonify({"status": result.stderr}), 500

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()
        query = f"UPDATE eth0_setting SET mode0 = ?,ip0 = ?,netmask0 = ?,network0 = ?,gateway0 = ?"
        cursor.execute(query, (mode0, ip0, netmask0, network0, gateway0))
        conn.commit()

        query1 = f"UPDATE eth1_setting SET mode1 = ?,ip1 = ?,netmask1 = ?,network1 = ?,gateway1 = ?,mark = ?"
        cursor.execute(query1, (mode1, ip1, netmask1, network1, gateway1, mark))
        conn.commit()

        cursor.close()
        conn.close()

        return jsonify({"status": "Set successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/restartNetwork", methods=["GET"])  # 採用多線程 避免整個程式卡住
def restartNetwork():
    try:
        networkProcess = multiprocessing.Process(target=networkRestart_process)
        networkProcess.start()

        return jsonify({"status": "Restarting..."})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


def networkRestart_process():  # 重啟網路
    restart = ["sudo", "/usr/share/apache2/default-site/htdocs/network.sh"]
    subprocess.run(restart, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)


@app.route("/getEthernet", methods=["GET"])  # 從database取得網路接口配置
def getEthernet():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM eth0_setting")
        eth0 = cursor.fetchone()

        cursor.execute("SELECT * FROM eth1_setting")
        eth1 = cursor.fetchone()

        cursor.close()
        conn.close()

        result = {
            "mode0": eth0[0],
            "ip0": eth0[1],
            "netmask0": eth0[2],
            "network0": eth0[3],
            "gateway0": eth1[4],
            "mode1": eth1[0],
            "ip1": eth1[1],
            "netmask1": eth1[2],
            "network1": eth1[3],
            "gateway1": eth1[4],
            "mark": eth1[5],
        }
        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/setSerial", methods=["POST"])  # 設定serial 先將配置寫入database中 再寫入本地檔案內
def setSerial():
    try:
        data = request.json
        baudrate = data.get("uart_baudrate_p1")
        char_timeout = data.get("uart_char_tmout_ms_p1")
        cmd_interval = data.get("uart_cmd_interval_ms_p1")
        databits = data.get("uart_databits_p1")
        enable = data.get("uart_enable_p1")
        name = data.get("uart_name_p1")
        parity = data.get("uart_parity_p1")
        stopbits = data.get("uart_stopbits_p1")
        total_timeout = data.get("uart_total_tmout_ms_p1")
        Type = data.get("uart_type_p1")

        baudrate2 = data.get("uart_baudrate_p2")
        char_timeout2 = data.get("uart_char_tmout_ms_p2")
        cmd_interval2 = data.get("uart_cmd_interval_ms_p2")
        databits2 = data.get("uart_databits_p2")
        enable2 = data.get("uart_enable_p2")
        name2 = data.get("uart_name_p2")
        parity2 = data.get("uart_parity_p2")
        stopbits2 = data.get("uart_stopbits_p2")
        total_timeout2 = data.get("uart_total_tmout_ms_p2")
        Type2 = data.get("uart_type_p2")

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        query = f"UPDATE tty1_setting SET baudrate = ?,char_timeout = ?,cmd_interval = ?,databits = ?,enable = ?,name = ?,parity = ?,stopbits = ?,total_timeout = ?,type = ?"
        cursor.execute(
            query,
            (
                baudrate,
                char_timeout,
                cmd_interval,
                databits,
                enable,
                name,
                parity,
                stopbits,
                total_timeout,
                Type,
            ),
        )
        conn.commit()

        query = f"UPDATE tty2_setting SET baudrate = ?,char_timeout = ?,cmd_interval = ?,databits = ?,enable = ?,name = ?,parity = ?,stopbits = ?,total_timeout = ?,type = ?"
        cursor.execute(
            query,
            (
                baudrate2,
                char_timeout2,
                cmd_interval2,
                databits2,
                enable2,
                name2,
                parity2,
                stopbits2,
                total_timeout2,
                Type2,
            ),
        )
        conn.commit()

        cursor.close()
        conn.close()

        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_tty.txt",
            "r",
        ) as file:
            lines = file.readlines()
        for key, value in data.items():
            new_line = f"{key}={value}\n"
            for i, line in enumerate(lines):
                if line.startswith(key + "="):
                    lines[i] = new_line
                    break
        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_tty.txt",
            "w",
        ) as file:
            file.writelines(lines)
        # 用多線程重啟DC 以讓新設定生效
        dcProcess = multiprocessing.Process(target=dcRestart_process)
        dcProcess.start()
        return jsonify({"status": "Set successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getSerial", methods=["GET"])  # 從database取得serial配置
def getSerial():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM tty1_setting")
        p1 = cursor.fetchone()
        cursor.execute("SELECT * FROM tty2_setting")
        p2 = cursor.fetchone()
        cursor.close()
        conn.close()

        result = {
            "uart_baudrate_p1": p1[0],
            "uart_baudrate_p2": p2[0],
            "uart_char_tmout_ms_p1": p1[1],
            "uart_char_tmout_ms_p2": p2[1],
            "uart_cmd_interval_ms_p1": p1[2],
            "uart_cmd_interval_ms_p2": p2[2],
            "uart_databits_p1": p1[3],
            "uart_databits_p2": p2[3],
            "uart_enable_p1": p1[4],
            "uart_enable_p2": p2[4],
            "uart_name_p1": p1[5],
            "uart_name_p2": p2[5],
            "uart_parity_p1": p1[6],
            "uart_parity_p2": p2[6],
            "uart_stopbits_p1": p1[7],
            "uart_stopbits_p2": p2[7],
            "uart_total_tmout_ms_p1": p1[8],
            "uart_total_tmout_ms_p2": p2[8],
            "uart_type_p1": p1[9],
            "uart_type_p2": p2[9],
        }
        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/setMQTT", methods=["POST"])  # 設定MQTT 寫入database + 檔案內
def setMQTT():
    try:
        data = request.json
        scan = data.get("mb_scan_enable")
        master_interval = data.get("mb_tcp_master_interval_ms")
        master_timeout = data.get("mb_tcp_master_resp_tmout")
        slave_enable = data.get("mb_tcp_slave_enable")
        slave_port = data.get("mb_tcp_slave_listen_port")
        hostname = data.get("mqtt_broker_hostname")
        port = data.get("mqtt_broker_port")
        password = data.get("mqtt_password")
        topic_data = data.get("mqtt_topic_data")
        topic_dbg = data.get("mqtt_topic_dbg")
        username = data.get("mqtt_username")

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        query = f"UPDATE device_setting SET scan = ?,master_interval = ?,master_timeout = ?,slave_enable = ?,slave_port = ?,hostname = ?,port = ?,password = ?,topic_data = ?,topic_dbg = ?,username = ?"
        cursor.execute(
            query,
            (
                scan,
                master_interval,
                master_timeout,
                slave_enable,
                slave_port,
                hostname,
                port,
                password,
                topic_data,
                topic_dbg,
                username,
            ),
        )
        conn.commit()
        cursor.close()
        conn.close()

        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_device.txt",
            "r",
        ) as file:
            lines = file.readlines()
        for key, value in data.items():
            new_line = f"{key}={value}\n"
            for i, line in enumerate(lines):
                if line.startswith(key + "="):
                    lines[i] = new_line
                    break
        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_device.txt",
            "w",
        ) as file:
            file.writelines(lines)

        # 用多線程重啟DC 以讓新設定生效
        dcProcess = multiprocessing.Process(target=dcRestart_process)
        dcProcess.start()
        return jsonify({"status": "Set successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getMQTT", methods=["GET"])  # 從database取得mqtt配置
def getMQTT():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM device_setting")
        device = cursor.fetchone()

        result = {
            "mb_scan_enable": device[0],
            "mb_tcp_master_interval_ms": device[1],
            "mb_tcp_master_resp_tmout": device[2],
            "mb_tcp_slave_enable": device[3],
            "mb_tcp_slave_listen_port": device[4],
            "mqtt_broker_hostname": device[5],
            "mqtt_broker_port": device[6],
            "mqtt_password": device[7],
            "mqtt_topic_data": device[8],
            "mqtt_topic_dbg": device[9],
            "mqtt_username": device[10],
        }
        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/setGateway", methods=["POST"])  # 配置gateway 寫入database + 文件
def setGateway():
    try:
        data = request.json
        enable = data.get("gateway_enable")
        port = data.get("gateway_listen_port")
        p1_max = data.get("uart_rtu_addr_max_p1")
        p2_max = data.get("uart_rtu_addr_max_p2")
        p1_min = data.get("uart_rtu_addr_min_p1")
        p2_min = data.get("uart_rtu_addr_min_p2")

        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        query = f"UPDATE gateway_setting SET enable = ?,port = ?,p1_max = ?,p2_max = ?,p1_min = ?,p2_min = ?"
        cursor.execute(query, (enable, port, p1_max, p2_max, p1_min, p2_min))
        conn.commit()
        cursor.close()
        conn.close()

        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_gateway.txt",
            "r",
        ) as file:
            lines = file.readlines()
        for key, value in data.items():
            new_line = f"{key}={value}\n"
            for i, line in enumerate(lines):
                if line.startswith(key + "="):
                    lines[i] = new_line
                    break
        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_gateway.txt",
            "w",
        ) as file:
            file.writelines(lines)

        # 用多線程重啟DC 以讓新設定生效
        dcProcess = multiprocessing.Process(target=dcRestart_process)
        dcProcess.start()
        return jsonify({"status": "Set successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/getGateway", methods=["GET"])  # 從database中取得配置
def getGateway():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM gateway_setting")
        gateway = cursor.fetchone()

        result = {
            "gateway_enable": gateway[0],
            "gateway_listen_port": gateway[1],
            "uart_rtu_addr_max_p1": gateway[2],
            "uart_rtu_addr_max_p2": gateway[3],
            "uart_rtu_addr_min_p1": gateway[4],
            "uart_rtu_addr_min_p2": gateway[5],
        }
        return jsonify(result)
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


@app.route("/reload", methods=["GET"])  # 重啟DC 會一併從database中重寫register的檔案
def reload():
    try:
        conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/device.db")
        cursor = conn.cursor()

        # 取得所有的table名稱
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table_names = cursor.fetchall()

        # 開啟txt文件以重寫
        with open(
            "/usr/share/apache2/default-site/htdocs/device_center_rls/reg_list.txt", "w"
        ) as f:
            # 遍歷所有table
            for table_name in table_names:
                table_name = table_name[0]

                # 取得table中的所有資料
                cursor.execute(f"SELECT * FROM {table_name};")
                rows = cursor.fetchall()

                # 將每一列的資料寫入txt
                for row in rows:
                    row_values = [
                        str(value) if value is not None else "" for value in row
                    ]
                    f.write(",".join(row_values) + "\n")

        cursor.close()
        conn.close()
        # 用多線程重啟DC 以讓新設定生效
        dcProcess = multiprocessing.Process(target=dcRestart_process)
        dcProcess.start()
        return jsonify({"status": "Reload successful!"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


def dcRestart_process():  # 重新DC的線程
    restartDC = ["/etc/init.d/device_center.sh", "restart"]
    subprocess.run(restartDC, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)


@app.route("/pause", methods=["GET"])  # 暫停採集
def pause():
    stopDC = ["/etc/init.d/device_center.sh", "stop"]
    result = subprocess.run(
        stopDC, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
    )

    if result.returncode == 0:
        return jsonify({"status": "Stop successful!"})
    else:
        return jsonify({"status": result.stderr})


@app.route("/start", methods=["GET"])  # 開始採集
def start():
    startDC = ["/etc/init.d/device_center.sh", "start"]
    result = subprocess.run(
        startDC, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
    )

    if result.returncode == 0:
        return jsonify({"status": "Start successful!"})
    else:
        return jsonify({"status": result.stderr})


@app.route("/export", methods=["GET"])  # 導出所有database並壓縮 提供給前端下載
def export():
    try:
        # 創建臨時資料夾
        temp_folder = "/usr/share/apache2/default-site/htdocs/temp_file"
        os.makedirs(temp_folder, exist_ok=True)

        # 複製檔案到臨時資料夾
        files_to_copy = [
            "/usr/share/apache2/default-site/htdocs/device.db",
            "/usr/share/apache2/default-site/htdocs/data.db",
            "/usr/share/apache2/default-site/htdocs/dashboard.db",
        ]
        for file in files_to_copy:
            # shutil.copy(file, os.path.join(temp_folder, file))
            shutil.copy(file, temp_folder)

        clear_mqtt_data()

        # 壓縮臨時資料夾成 ZIP 檔案
        zip_filename = "/usr/share/apache2/default-site/htdocs/exported_files.ts"
        with zipfile.ZipFile(zip_filename, "w") as zipf:
            for root, _, files in os.walk(temp_folder):
                for file in files:
                    zipf.write(
                        os.path.join(root, file),
                        os.path.relpath(os.path.join(root, file), temp_folder),
                    )

        # 發送壓縮檔案給前端下載
        response = send_file(zip_filename, as_attachment=True)

        # 刪除臨時資料夾和壓縮檔案
        # os.remove(zip_filename)
        shutil.rmtree(temp_folder)

        return response
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


def clear_mqtt_data():  # 由於導出是要搬遷設備用的 會將導出mqtt_data的表格清空
    conn = sqlite3.connect(
        os.path.join("/usr/share/apache2/default-site/htdocs/temp_file", "data.db")
    )
    cursor = conn.cursor()

    # 執行 SQL 命令來清空 mqtt_data 表格
    cursor.execute("DELETE FROM mqtt_data")

    conn.commit()
    conn.close()


@app.route("/import", methods=["POST"])  # 導入後會直接重寫本地的檔案
def import_data():
    try:
        # 獲取上傳的檔案
        uploaded_file = request.files["file"]

        if uploaded_file:
            # 將上傳的檔案儲存到臨時資料夾
            temp_folder = "/usr/share/apache2/default-site/htdocs/temp_import"
            os.makedirs(temp_folder, exist_ok=True)
            uploaded_filename = secure_filename(uploaded_file.filename)
            uploaded_file_path = os.path.join(temp_folder, uploaded_filename)
            uploaded_file.save(uploaded_file_path)

            # 解壓縮檔案
            with zipfile.ZipFile(uploaded_file_path, "r") as zipf:
                zipf.extractall(temp_folder)

            # 覆蓋指定路徑的檔案
            overwrite_files = [
                "device.db",
                "data.db",
                "dashboard.db",
            ]
            for file in overwrite_files:
                source_file = os.path.join(temp_folder, file)
                target_file = os.path.join(
                    "/usr/share/apache2/default-site/htdocs/", file
                )
                shutil.copy(source_file, target_file)

            # 刪除臨時資料夾
            shutil.rmtree(temp_folder)

            # 處理上傳後的database，將他寫入到本地文件中
            conn = sqlite3.connect("/usr/share/apache2/default-site/htdocs/data.db")
            cursor = conn.cursor()

            cursor.execute("SELECT * FROM gateway_setting")
            gateway = cursor.fetchone()
            cursor.execute("SELECT * FROM device_setting")
            device = cursor.fetchone()
            cursor.execute("SELECT * FROM tty1_setting")
            tty1 = cursor.fetchone()
            cursor.execute("SELECT * FROM tty2_setting")
            tty2 = cursor.fetchone()

            cursor.close()
            conn.close()

            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_gateway.txt",
                "r",
            ) as file:
                gateway_lines = file.readlines()

            gateway_config = {}

            for line in gateway_lines:
                key, value = line.strip().split("=")
                gateway_config[key] = value

            gateway_config["uart_rtu_addr_min_p1"] = gateway[4]
            gateway_config["uart_rtu_addr_max_p1"] = gateway[2]
            gateway_config["uart_rtu_addr_min_p2"] = gateway[5]
            gateway_config["uart_rtu_addr_max_p2"] = gateway[3]
            gateway_config["gateway_enable"] = gateway[0]
            gateway_config["gateway_listen_port"] = gateway[1]

            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_gateway.txt",
                "w",
            ) as file:
                for key, value in gateway_config.items():
                    file.write(f"{key}={value}\n")
            # gateway end
            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_device.txt",
                "r",
            ) as file:
                device_lines = file.readlines()

            device_config = {}

            for line in device_lines:
                key, value = line.strip().split("=")
                device_config[key] = value

            device_config["mb_tcp_master_interval_ms"] = device[1]
            device_config["mb_tcp_master_resp_tmout"] = device[2]
            device_config["mb_tcp_slave_enable"] = device[3]
            device_config["mb_tcp_slave_listen_port"] = device[4]
            device_config["mb_scan_enable"] = device[0]
            device_config["mqtt_broker_hostname"] = device[5]
            device_config["mqtt_broker_port"] = device[6]
            device_config["mqtt_username"] = device[10]
            device_config["mqtt_password"] = device[7]
            device_config["mqtt_topic_dbg"] = device[9]
            device_config["mqtt_topic_data"] = device[8]

            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_device.txt",
                "w",
            ) as file:
                for key, value in device_config.items():
                    file.write(f"{key}={value}\n")
            # device end

            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_tty.txt",
                "r",
            ) as file:
                tty_lines = file.readlines()

            tty_config = {}

            for line in tty_lines:
                key, value = line.strip().split("=")
                tty_config[key] = value

            tty_config["uart_enable_p1"] = tty1[4]
            tty_config["uart_name_p1"] = tty1[5]
            tty_config["uart_type_p1"] = tty1[9]
            tty_config["uart_baudrate_p1"] = tty1[0]
            tty_config["uart_parity_p1"] = tty1[6]
            tty_config["uart_databits_p1"] = tty1[3]
            tty_config["uart_stopbits_p1"] = tty1[7]
            tty_config["uart_char_tmout_ms_p1"] = tty1[1]
            tty_config["uart_total_tmout_ms_p1"] = tty1[8]
            tty_config["uart_cmd_interval_ms_p1"] = tty1[2]

            tty_config["uart_enable_p2"] = tty2[4]
            tty_config["uart_name_p2"] = tty2[5]
            tty_config["uart_type_p2"] = tty2[9]
            tty_config["uart_baudrate_p2"] = tty2[0]
            tty_config["uart_parity_p2"] = tty2[6]
            tty_config["uart_databits_p2"] = tty2[3]
            tty_config["uart_stopbits_p2"] = tty2[7]
            tty_config["uart_char_tmout_ms_p2"] = tty2[1]
            tty_config["uart_total_tmout_ms_p2"] = tty2[8]
            tty_config["uart_cmd_interval_ms_p2"] = tty2[2]

            with open(
                "/usr/share/apache2/default-site/htdocs/device_center_rls/settings_tty.txt",
                "w",
            ) as file:
                for key, value in tty_config.items():
                    file.write(f"{key}={value}\n")

            return jsonify({"status": "Import Successful!"})
        else:
            return jsonify({"status": "No file uploaded"})
    except Exception as e:
        print("Error:", e)
        return jsonify({"error": str(e)}), 500


def remove_symbol_from_value(value): # 因為本地文件可以設置單位 但前端apexchart不支持這樣 所以送data之前要先移除非數字的部分
    # 使用正則表達式匹配數字和符號部分
    pattern = r"([0-9.]+).*"
    match = re.match(pattern, value)

    if match:
        # 取出匹配到的數字部分
        numeric_value = match.group(1)
        return numeric_value
    else:
        # 如果沒有匹配到，直接返回原來的值
        return value


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
