<br />
<h1 align="center">Data Collector (backend)</h1>

## Apache2

- URL Rewriting

## Python

- 以多進程的方式運行，為前端提供API。
- 使用SocketIO提供實時數據。
- 讀寫SQLite3資料庫

## 技術堆疊
- SQLite
- Flask
- Multiprocessing
- SocketIO