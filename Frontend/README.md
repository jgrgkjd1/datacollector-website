<br />
<h1 align="center">Data Collector (frontend)</h1>

## 特點

- **可視化界面：** 將 CLI 應用的配置文件透過網頁呈現。
- **數據監控：** 提供兩種Dashboard的呈現方式來展示一天內的歷史數據、即時數據。
- **快速搬遷：** 原先應用為多個配置檔組成，透過網頁配置則都會寫入資料庫內，以 Data Book 的方式創建歸類，同時也支援導出/導入。
- **警訊通知：** 透過 Line Notify 在應用採集到無回應時發送通知提醒。

## 技術堆疊

- Vue.js (Use router)
- TypeScript
- Vite
- Vuetify
- Element-plus
- Vue3 Apexcharts
- Socket.io
- Vxe-table
