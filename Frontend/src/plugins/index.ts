/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import { loadFonts } from "./webfontloader";
import vuetify from "./vuetify";
import router from "../router";
import ElementPlus from "element-plus";
import "element-plus/theme-chalk/index.css";
import VXETable from "vxe-table";
import "vxe-table/lib/style.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

// Types
import type { App } from "vue";

export function registerPlugins(app: App) {
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
  }
  loadFonts();
  app.use(vuetify).use(router).use(VXETable).use(ElementPlus);
}
