// Composables
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Dashboard",
        component: () => import("@/layouts/default/DashboardPage.vue"),
      },
    ],
  },
  {
    path: "/SetDashboard",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "SetDashboard",
        component: () => import("@/layouts/default/SetDashboard.vue"),
      },
    ],
  },
  {
    path: "/Device",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Device",
        component: () => import("@/layouts/default/DevicePage.vue"),
      },
    ],
  },
  {
    path: "/Notify",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Notify",
        component: () => import("@/layouts/default/NotifyPage.vue"),
      },
    ],
  },
  {
    path: "/Advanced",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Advanced",
        component: () => import("@/layouts/default/AdvancedPage.vue"),
      },
    ],
  },
  {
    path: "/Hardware",
    component: () => import("@/layouts/default/Default.vue"),
    children: [
      {
        path: "",
        name: "Hardware",
        component: () => import("@/layouts/default/Hardware.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
